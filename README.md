# compiler_tester

Compiler tester automatically tests a given Fortran compiler for features.

# Links

Some related discussions and issues:

* https://gitlab.com/lfortran/lfortran/-/issues/568
* https://github.com/j3-fortran/fortran_proposals/issues/57
